Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DRC
Upstream-Contact: Denis Sbragion <d.sbragion@infotecna.it>
Source: https://drc-fir.sourceforge.io/
Copyright: 2002-2010 Denis Sbragion <d.sbragion@infotecna.it>
Comment: The original tarball was repacked in order to remove prebuild
 Windows binaries and HTML documentation (which is not built from source).
Files-Excluded:
 doc
 sample/*.exe

Files: *
Copyright:
 2002-2010 Denis Sbragion <d.sbragion@infotecna.it>
 1999-2002 Yuuki NINOMIYA <gm@debian.or.jp>
 1996-2001 Takuya OOURA
 1997-2000, 2004 Gerard Jungman, Brian Gough
 1996-2001 Brian Gough
 1996-2000, 2004 Jorma Olavi Tähtinen, Brian Gough
 1996-2000 Gerard Jungman
License: GPL-2+

Files: source/MLeaks/*
Copyright: 2004-2008 Wu Yongwei <adah at users dot sourceforge dot net>
License: Zlib

Files: source/minIni/*
Copyright: 2008-2012 CompuPhase
License: Apache-2.0

Files: source/getopt/getopt1.c
 source/getopt/getopt.c
 source/getopt/getopt.h
Copyright: 1987-1997 Free Software Foundation, Inc
License: LGPL-2+

Files: source/fftsg.h
Copyright: Takuya OOURA, 1996-2001
License: other-1

Files: debian/*
Copyright:
 2011-2016 Jaromír Mikeš <mira.mikes@seznam.cz>
 2011 Alessio Treglia <alessio@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General Public
 License can be found in `/usr/share/common-licenses/LGPL-2'.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
    not claim that you wrote the original software.  If you use this
    software in a product, an acknowledgement in the product
    documentation would be appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 3. This notice may not be removed or altered from any source
    distribution.

License: Apache-2.0
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at
 .
      https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache Software License version 2 can
 be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: other-1
 You may  use, copy, modify  and  distribute  this code  for any
 purpose (include commercial use) and without fee.